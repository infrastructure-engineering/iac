# IaC (Infrastructure as Code) my notes


If you want to create an infrastructure, you either need to configure and install everything manually, or you need to write a code that makes them automatic(a script or programing language).
#### as follows;
```bash:
#!/bin/bash

#install packages
apt install docker

apt install minikube
apt install kubectl

#start services
systemctl start docker
systemctl start minikube

#deploy project
kubectl create deployment hello-node --image=k8s.gcr.io/echoserver:1.4
....
....
....

```

This is where the concept of IaC comes into play. Instead of writing code, we can set up the entire infrastructure by writing a simple file and run if running IaC software.

### IaC softwares for Linux(I don`t know there is for windows)

- **Terraform**
- **Ansible**
- AWS CloudFormation
- Azure Resource Manager
- Google Cloud Deployment Manager
- Chef
- Puppet
- SaltStack
- Vagrant

okay I will search and use Terraform and Ansible.

